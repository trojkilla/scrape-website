#!/usr/bin/env node
'use strict';

const puppeteer = require('puppeteer');
const CHC = require('chrome-har-capturer');
const cheerio = require('cheerio')
const fs = require('fs');
const { promisify } = require('util');

var ArgumentParser = require('argparse').ArgumentParser;
var parser = new ArgumentParser({
  version: '1.2',
  addHelp: true,
  description: 'Open URL with a browser'
});

parser.addArgument([ '--delay' ], {
  help: 'How many milliseconds to wait after opening the url',
  type: 'int',
});

parser.addArgument([ '--process-timeout' ], {
  help: 'How long to wait until Chromium should be killed',
  type: 'int',
  defaultValue: 30000,
});

parser.addArgument([ '--resolution' ], {
  help: 'Resolution of the screenshot, default is 1366x768',
  defaultValue: "1366x768"
});

parser.addArgument([ '--full-screenshot' ], {
  help: 'Capture a full screenshot of the page',
  action: 'storeTrue'
});

parser.addArgument([ '--output-har', '--har' ], {
  help: 'Output HTTP Archive file(s)',
  action: 'storeTrue'
});

parser.addArgument([ '--output-png', '--png' ], {
  help: 'Output a png screenshot of the URL(s)',
  action: 'storeTrue'
});

parser.addArgument([ '--url' ], {
  help: 'Single url to scrape'
});

parser.addArgument([ '--url-file' ], {
  help: 'JSON file to read urls from, example: { "url_list": [ "http://example.com", "..." ] }'
});

parser.addArgument([ '-o', '--output-dir' ], {
  help: 'Directory to output files to, default is CWD',
  defaultValue: "."
});

parser.addArgument([ '--user-agent' ], {
  help: 'User-agent of the browser',
  defaultValue: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36"
});

parser.addArgument([ '--chromium-args' ], {
  help: 'Arguments to pass on to the Chromium process, example: --chromium-args="--proxy=socks5://172.18.0.2:9050"'
});

parser.addArgument([ '--crawl-depth' ], {
  help: 'Crawl found URLs for depth N',
  type: 'int',
  defaultValue: 0,
});

var args = parser.parseArgs();

args.puppeteerOptions = {
  executablePath: process.env.CHROME_BIN || null,
  args: [
    '--no-sandbox',
    '--disable-setuid-sandbox',
    '--headless',
    '--disable-gpu',
    '--disable-dev-shm-usage',
  ],
  devtools: false,
  headless: true,
  ignoreHTTPSErrors: true,
  pipe: true, // TODO: does this help?
};

if (!(args.url_file || args.url)) {
    parser.error("Give --url or --url-file as argument")
    parser.printHelp();
    process.exit(1);
}

if (!args.output_har && !args.output_png) {
    args.output_har = true;
    args.output_png = true;
}

if (args.chromium_args) {
  let concatArgs = args.puppeteerOptions.args.concat(args.chromium_args.split(','))
  args.puppeteerOptions.args = concatArgs;
}

function sleep(ms) {
    ms = (ms) ? ms : 0;
    return new Promise(resolve => {setTimeout(resolve, ms);});
}

function processTimeout(pid, ms, stdout) {
    ms = (ms) ? ms : 0;
    const err_logging = (stdout) => {

        try {
            process.kill(pid, 'SIGTERM');
        } catch (e) {
            stdout['logs'] = String(e);
        }
        console.log(JSON.stringify(stdout));
    }

    return setTimeout(err_logging, ms, stdout);
}

async function finish(url, events, har_path) {
    try {
        // Ignore "Incomplete event log" errors from library
        const har = await CHC.fromLog(url, events, { content: true }).catch(e => {});
        await promisify(fs.writeFile)(har_path, JSON.stringify(har));
        return har;

    } catch (e) {
        console.error(e.message);
    }
}

process.on('uncaughtException', (error) => {
    console.error(error);
    process.exit(1);
});

process.on('unhandledRejection', (reason, p) => {
    console.error(reason, p);
    process.exit(1);
});

if (args.output_dir) {
    if (!fs.existsSync(args.output_dir)){
        fs.mkdirSync(args.output_dir);
    }
}

let crawledURLs = []

if (args.url_file) {
    const metadata = JSON.parse(fs.readFileSync(args.url_file, 'utf8'));
    const url_list = metadata.url_list;
    for (let index = 0; index < url_list.length; ++index) {
        scrape(url_list[index], args);
    }
}

if (args.url) {
    scrape(args.url, args);
}

async function build_har_client(args, page, har_path) {
    // Code borrowed from issue:
    // https://github.com/cyrus-and/chrome-har-capturer/issues/75

    const client = await page.target().createCDPSession();

    let counter = 0;
    let events = [];

    const watchedEvents = [
        'Network.dataReceived',
        'Network.loadingFailed',
        'Network.loadingFinished',
        'Network.requestWillBeSent',
        'Network.resourceChangedPriority',
        'Network.responseReceived',
        'Page.domContentEventFired',
        'Page.loadEventFired'
    ];

    await client.send('Page.enable');
    await client.send('Network.enable');

    watchedEvents.forEach(method => {
        client.on(method, params => {
            events.push({method, params});
        });
    });

    await client.on('Network.loadingFinished', async ({requestId}) => {
        // call Network.getResponsebody manually for each
        // Network.loadingFinished events
        counter++;
        const params = await client.send('Network.getResponseBody', {requestId});
        counter--;

        // build the synthetic events
        const {body, base64Encoded} = params;
        events.push({
            method: 'Network.getResponseBody',
            params: {
                requestId,
                body,
                base64Encoded
            }
        });

        // when there are no more pending invocations build the HAR
        if (counter === 0) {
            const har = await finish(args.url, events, har_path);
        }
    });

    return client;
}

async function open_page(page_url, browser, args) {
    const http_uri_scheme = new RegExp("^[a-z]+://")

    if (!http_uri_scheme.test(page_url)) {
        console.info("No URI Scheme supplied, assuming https://");
        page_url = "https://" + page_url
    }

    args.url = page_url
    args.hostname = new URL(page_url).hostname;

    let now = new Date();
    args.dateStr = now.toISOString().replace(/:/g, '_');
    args.delay = parseInt(args.delay, 10);
    [args.width, args.height] = args.resolution.split('x').map(v => parseInt(v, 10));

    let stdout = {
        url: page_url,
        date: args.dateStr,
        timestamp: Math.floor(now.getTime() / 1000),
        width: args.width,
        height: args.height
    };

    const pid = browser.process().pid;
    const browserTimeout = processTimeout(pid, args.process_timeout, stdout);

    const page = await browser.newPage();

    page.setViewport({
        width: args.width,
        height: args.height
    });

    page.setUserAgent(args.user_agent);

    const har_path = `${args.output_dir}/${args.hostname}_${args.dateStr}.har`;
    stdout['har_output'] = har_path

    const client = await build_har_client(args, page, har_path);

    let html;
    try {
        await page.goto(page_url, {waitUntil: 'networkidle0', timeout: 0});
        html = await page.content();
        await sleep(args.delay);
    } catch (e) {
        stdout['urls'] = []
        stdout['logs'] = String(e);
        console.log(JSON.stringify(stdout));
        return stdout;
    }

    if (args.output_png) {
        let png = `${args.output_dir}/${args.hostname}_${args.width}_${args.height}_${args.dateStr}.png`;
        await page.screenshot({path: `${png}`, fullPage: args.full_screenshot});
        stdout['png_output'] = png
    }

    console.log(JSON.stringify(stdout));
    await client.detach();
    await page.close();

    clearTimeout(browserTimeout);

    stdout['urls'] = []

    const $ = cheerio.load(html);
    const links = $('a');
    $(links).each(function(i, link) {
        try {
            const parsed = new URL($(link).attr('href'), page_url);
            if (!parsed.protocol.startsWith("http")) {
                return;
            }

            stdout['urls'].push(parsed);

        } catch (e) {}
    });

    return stdout;
}

async function scrape(scrape_url, args) {
    const browser = await puppeteer.launch({
        ...args.puppeteerOptions
    });

    args.depth = 0;
    // Initial entry to crawl tree
    const result = await open_page(scrape_url, browser, args)
    const url_list = result['urls']

    while (url_list && url_list.length !== 0) {

        if (args.depth > args.crawl_depth) {
            break;
        }

        args.depth++;

        const child_url = url_list.shift()
        if (crawledURLs.includes(child_url)) {
            continue;
        }

        crawledURLs.push(child_url);

        var clone = Object.assign({}, args);
        const child_result = await open_page(child_url, browser, clone);
        if (child_result['urls']) {
            url_list.push(...child_result['urls'])
        }
    }

    await browser.close();
}
