FROM node:13.10.1-alpine3.11

ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/ \
    PUPPETEER_SKIP_CHROMIUM_DOWNLOAD="true"

RUN apk add --no-cache chromium==79.0.3945.130-r0

ADD "package.json" .
ADD "yarn.lock" .

RUN yarn install --frozen-lockfile \
    && yarn add cheerio \
    && yarn cache clean

RUN deluser --remove-home node \
    && adduser -D appuser \
    && mkdir -p /home/appuser/tool \
    && chown -R appuser:appuser /home/appuser

USER appuser
WORKDIR /home/appuser/tool

ADD "scrape-website.js" .
ENTRYPOINT ["node", "scrape-website.js"]
